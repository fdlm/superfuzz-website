+++
author = "Filip Korzeniowski"
title = "Hi, I'm Filip. How Are You?"
date = "2021-01-01"
tags = []
+++
<center>

![me](img/me.jpg)

</center>

I teach computers to understand music.

I also climb rocks, cycle gravel, and surf whitewater. 

Sometimes I even play music, and very rarely, I record it:  
[Plug In Baby](https://www.youtube.com/watch?v=-GCWHrDZ5nc),
[Soul To Squeeze](https://www.youtube.com/watch?v=z67cRf-0fHw).

[Talk to me](mailto:2020@superfuzz.me) about rock climbing, surfing, yoga, weight lifting, and Doom Metal. Or, about deep learning, probabilistic models, and Python.

This is what I'm doing [now](/now)....
