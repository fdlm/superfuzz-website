---
title: Vegan Chorizo for Health and Profit
date: 2021-11-07
tags: [ food ]
---

One thing I'm missing deeply from California are delicious vegan chorizos.
The alternatives I found in Vienna suck donkey balls, so I had to do it myself. It's actually pretty easy, once you find the key ingredient: **vegan ground meat.**

The best I found is based on pea protein and sold by **Hofer**, a cheap supermarket from Germany. Funny enough, the owner of Hofer also owns **Trader Joe's**, my favorite supermarket in the US. 

In any case, once you have the vegan ground meat, the process is pretty simple. 

## Ingredients

|        |                    |
|:------ | ------------------:|
| 200 g  |  Vegan ground meat |
| 1      |        Small onion |
| 3.8 g  |               Salt |
| 1.25 g | Ancho chile powder |
| 0.7 g  |            Paprika |
| 0.7 g  |     Cayenne pepper |
| 0.8 g  |       Ground cumin |
| 1      |       Garlic clove |
| 1.6 g  |            Oregano |
| "some" |       Tomato paste | 

But wait, do I need to use the precise amounts of chile powder, paprika, and so on? **Yes**, for the love of the chorizo god. (No.)

## Process
1. Start caramelizing the onion: Put some olive oil into a skillet, small dice the onion, add onion, wait.
2. Mix the vegan ground meat with all other ingredients. Use your hands. It feels good.
3. Once onion is caramelized, put everything in the skillet and cook for 5-8 minutes, until done. Do not cook for too long, otherwise the chorizo will become dry.

That's all, folks! I put my chorizo immediately into a Taco. It's so good, I don't even want to know how to store it.