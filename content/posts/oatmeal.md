---
title: Supercharge Your Breakfast
date: 2021-01-01T13:16:59+01:00
draft: false
tags: [ ]
---

How to get out of bed every morning? With the super-powers of oatmeal! I've been optimizing the following recipe for close to a year now, experimenting (i.e., eating it) almost daily. I love it.

The recipe is almost vegan (just substitute honey with, say, date syrup), and contains loads of vitamins and minerals people claim vegans lack. So, not only it gets you out of bed, it also gives you a head-start on precious nutrients for the day!

Finally, it's just amazingly delicious. Imagine eating cake for breakfast, every day, except that it's healthy. How awesome is that?

Without further ado, here we go:

## Ingredients
  
|||
| :------------------- |   ---: |
| Fortified plant milk |  250ml |
| Rolled Oats          |    60g |
| Basic Porridge Mix   |    15g |
| Flaxseed             |    10g |
| Crumbled Walnuts     |    15g |
| Banana               | 1 (80g) |
| Honey                | 1 Tbsp (15g) |
| Cinnamon Powder      | to taste     |

## Process

1. Pour the plant milk into a pot and set to medium heat.
2. Measure the rolled oats and porridge mix and add them to the milk, stirring once in a while.
3. While the stuff is heating up, put the flaxseeds and crumbled walnuts into your bowl.
4. Peel and slice the banana and put it into your bowl.
5. Add the honey into the pot and stir.
6. Once the oat/porridge/honey mix is hot, pour it into your bowl.
7. Add cinnamon. More. Still more. I love cinnamon.
8. Mix and devour the tastiest breakfast in the world.

## Nutrients

Now, you can feel good about providing your body with the following healthy 
building blocks of life:

|                  | Amount | Daily Rec. | Percentage |
| :---             |   ---: |       ---: |       ---: |
| Energy (kCal)    |  593   | 2500 |        23.7% |
| Protein (g)      |  16.6  | 75   |        22.2% |
| Carbs (g)        |  72.3  | -    |        - |
|  - Sugar (g)     |  26.1  | -    |        - |
| Fat (g)          |  22.8  | -    |        - |
| Omega-3 (g)      |  2.7   | 1.6  |        170% |
| B12 (µg)         |  0.95  | 2.4  |        39.6% |
| Iron (mg)        |  5.2  | 8  |        65% |
| Zinc (mg)        |  3.6 | 11  |        32.3% |
| Calcium (mg)     |  365.5 | 1000  |     36.6% |

You check off Omega-3, and 2/3 of your daily iron supply, and over-index
on all of these important minerals and vitamins, while savoring a super tasty
oatmeal breakfast!

The fine-print: I'm not a doctor nor nutritionist. For a detailed look at the
numbers, check [this spreadsheet](https://docs.google.com/spreadsheets/d/1h-2mXrCNA4U7Y4GGCZah5f25Zvo4lMUbdF2kDyShXDU/edit?usp=sharing).
