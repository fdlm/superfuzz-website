---
title: Now
date: 2021-01-01T13:20:54+01:00
lastMod: 2022-10-04
tags: [ nownownow ]
---

I'm living in Vienna, working remotely at [Moises.ai](https://moises.ai).

I'm getting more and more into cycling between road and gravel, pushing myself, my heart rate, and the tyre friction to the limits. Looking forward to doing a bike-packing tour later this year. My big dream is to take part in (and finish) the [Badlands gravel race](https://badlands.cc) next year.

Recently, I got back into weight training, after too long of a break. Lifting hurts, [but sometimes, it's a good hurt](https://www.youtube.com/watch?v=wGqFRHEh8iM), and it feels like I'm alive.
